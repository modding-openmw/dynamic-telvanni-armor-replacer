# Dynamic Telvanni Armor Replacer

Dynamically equips Telvanni NPCs with the Cephalopod Armor and, if Tamriel Data is installed, with the Mole Crab Armor.

There's plenty of mods that edit the Telvanni Guard NPCs to equip them with the awesome Cephalopod Armor instead of the vanilla Armun-An Bonemold, but unfortunately, there's still many mods this doesn't cover (chief amongst them being [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) and [Uvirith's Legacy](https://stuporstar.sarahdimento.com/)).

This mod scans all NPCs from all of your installed mods, and if it detects a Telvanni NPC wearing any of the Armun-An Bonemold armor pieces, it swaps all of the Bonemold Armor in their inventory for the Cephalopod Armor pieces.

As an added bonus when you have Tamriel Data installed, the mod is also able to detect if an NPC is wearing one of the Mole Crab helmets, and if they do, it swaps the Bonemold Armor for Mole Crab Armor instead.

**Requires OpenMW 0.49 or newer and at least one of the following mods:**

* **[Tamriel Data](https://www.tamriel-rebuilt.org/downloads/resources) - recommended for full functionality!**
* **[RR Mod Series - Better Telvanni Cephalopod Armor](https://www.nexusmods.com/morrowind/mods/44837)**
* **[Telvanni Cephalopod Armor](https://www.nexusmods.com/morrowind/mods/44062)**
* **[Yet Another Guard Diversity](https://www.nexusmods.com/morrowind/mods/45894)** (the **Full Cephalopod** option)

#### Credits

OpenMW-Lua script author: **Ronik**

Telvanni Cephalopod Armor authors: **Danke** and **Cheydin**

Tamriel_Data edits to the Cephalopod Armor: **Wolli** and **Cicero**

RR Mod Series - Better Telvanni Cephalopod Armor authors: **Siberian Crab**, **Darknut**, **Tiebrakre**, **Neuman**, and **Dereko**

Yet Another Guard Diversity authors: **Half11** and **SkoomaPro**

Telvanni Mole Crab Armor author: **???** (not included in Tamriel Data credits, please let me know if you know the author so I can give them credit)

**Special Thanks**:

* **johnnyhostile** for making [Mage Robes for OpenMW Lua](https://modding-openmw.gitlab.io/mage-robes-for-openmw-lua/), which this mod is based on
* **The OpenMW team, including every contributor** for making OpenMW and OpenMW-CS
* **The Modding-OpenMW.com team** for being amazing
* **All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server** for their dilligent testing <3
* **Bethesda** for making Morrowind

#### Web

[Project Home](https://modding-openmw.gitlab.io/dynamic-telvanni-armor-replacer/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/dynamic-telvanni-armor-replacer)

#### Installation

**OpenMW 0.49 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/dynamic-telvanni-armor-replacer/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Armor\dynamic-telvanni-armor-replacer

        # Linux
        /home/username/games/OpenMWMods/Armor/dynamic-telvanni-armor-replacer

        # macOS
        /Users/username/games/OpenMWMods/Armor/dynamic-telvanni-armor-replacer

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Armor\dynamic-telvanni-armor-replacer"`)
1. Add `content=dynamic-telvanni-armor.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Lua Interface

A Lua interface is provided to allow 3rd party mods to add mod plugins and actors to this mod's internal blacklists. An "addon" mod can be created that uses the interface provided by this one to add support for the desired IDs.

To do this, two files are needed:

1. `YourAddonName.omwscripts` with the following contents:

```
GLOBAL: scripts/YourAddonName/global.lua
```

1. `scripts/YourAddonName/global.lua` with the following contents:

```lua
local telvanniArmor = require("openmw.interfaces").DynamicTelvanniArmor

if not telvanniArmor then
    error("ERROR: Dynamic Telvanni Armor Replacer is not installed!")
end

telvanniArmor.AddActorToBlacklist({
        "actor_id_01",
        "actor_id_02",
        ...
})

telvanniArmor.AddModToBlacklist({
        "CoolMod1.esp",
        "RadMod2.esp",
        ...
})
```

File layout:

```
.
├── YourAddonName.omwscripts
└── scripts
    └── YourAddonName
        └── global.lua
```

Note you should change `YourAddonName` to match your mod's name and the IDs used to match the IDs you want to add.

The `telvanniArmor` variable gives you direct access to the Dynamic Telvanni Armor Replacer interface. Use `telvanniArmor.AddActorToBlacklist` to register new blacklisted actor IDs and `telvanniArmor.AddModToBlacklist` to register blacklisted mod plugin names. You can use whatever script and path names you like, but it must be [a global script](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#format-of-omwscripts).

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/dynamic-telvanni-armor-replacer/-/issues)
* Email `dynamic-telvanni-armor-replacer@modding-openmw.com`
* Contact the author on Discord: `@Ronik`
