local core = require("openmw.core")
local MOD_ID = "DynamicTelvanniArmor"
local L = core.l10n(MOD_ID)

-- Which content file will be used as the source for the armor.
Variant = 0

if core.API_REVISION < 51 then
    error(L("needNewerOpenMW"))
end

-- Check if we have any content files that we can use
if core.contentFiles.has("Tamriel_Data.esm") then
    Variant = 1
    print(L("haveTD"))
elseif core.contentFiles.has("RR_Danke's_Cephalopod_Armor_Eng.esp") or
    core.contentFiles.has("RR_Danke's_Cephalopod_Armor_Rus.esp") then
        Variant = 2
        print(L("haveRR"))
elseif core.contentFiles.has("Telvanni Cephalopod Armor.esp") or
    core.contentFiles.has("Yet Another Guard Diversity - Full Cephalopod.ESP") or
    core.contentFiles.has("Yet Another Guard Diversity - Telvanni (Full Cephalopod).ESP") then
        Variant = 3
        print(L("haveDanke"))
else
    error(L("missingDep"))
end
