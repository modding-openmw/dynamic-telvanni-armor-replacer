local self = require("openmw.self")

local equipped

local function startEquipProcess()
    equipped = self.type.equipment(self)
end

local function finishEquipProcess()
    self.type.setEquipment(self, equipped)
end

local function equipNewItem(params)
    if params.slot == 1 then
        equipped[self.type.EQUIPMENT_SLOT.Cuirass] = params.itemObj.recordId
    elseif params.slot == 2 then
        equipped[self.type.EQUIPMENT_SLOT.LeftPauldron] = params.itemObj.recordId
    elseif params.slot == 3 then
        equipped[self.type.EQUIPMENT_SLOT.RightPauldron] = params.itemObj.recordId
    elseif params.slot == 4 then
        equipped[self.type.EQUIPMENT_SLOT.Greaves] = params.itemObj.recordId
    elseif params.slot == 5 then
        equipped[self.type.EQUIPMENT_SLOT.Boots] = params.itemObj.recordId
    elseif params.slot == 9 then
        equipped[self.type.EQUIPMENT_SLOT.LeftGauntlet] = params.itemObj.recordId
    elseif params.slot == 10 then
        equipped[self.type.EQUIPMENT_SLOT.RightGauntlet] = params.itemObj.recordId
    end
end

return {eventHandlers = {
    momw_telvanniArmor_equipItem = equipNewItem,
    momw_telvanniArmor_startEquipProcess = startEquipProcess,
    momw_telvanniArmor_finishEquipProcess = finishEquipProcess
}}
