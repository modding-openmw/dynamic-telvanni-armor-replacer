require("scripts.dynamic-telvanni-armor.checks")
local world = require("openmw.world")

-- Mods to not give armor to.
local modBlacklist = {}

-- NPCs to not give armor.
local actorBlacklist = {}

-- A list of factions that will be checked to apply armor to. T_Mw_HouseTelvanni is deprecated but some mods might use it
local supportedFactions = {
    ["telvanni"] = true,
    ["t_mw_housetelvanni"] = true
}

-- A list of Mole Crab helmets
local moleCrabHelmets = {
    ["demon mole crab"] = true,
    ["devil mole crab helm"] = true,
    ["mole_crab_helm"] = true,
    ["T_De_FiendMoleCrabHelm_01"] = true
}

-- A list of NPCs that the script already did its magic for
local processedNPCs = {}

-- We'll store the current actor's equipment here
local equipped

-- If we find an Armun-An equipment piece, we'll swap the other bonemold pieces as well. Set this to false at start of every check
local foundArmun

-- If we find a Mole Crab helmet, we should swap the armor for Mole Crab armor instead. Set this to false at start of every check
local foundMoleCrab

-- Remove the thing from its container and delete it
local function deleteObj(obj)
    obj.enabled = false
    obj:remove(1)
end

-- 1 = Cuirass, 2 = Left Pauldron, 3 = Right Pauldron, 4 = Greaves, 5 = Boots, 9 = Left Gauntlet, 10 = Right Gauntlet, don't ask me why
local function findItem(equippedRec)
    local itemId

    if equippedRec.type == 1 then
        -- We don't care about anything but Armun-An, but if we find it, we should keep track of it
        if equippedRec.id ~= "bonemold_armun-an_cuirass" then
            return
        else
            foundArmun = true
        end
        -- Try to get the appropriate cuirass
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_Cuirass_01"
        elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_Cuirass_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_cuirass"
        elseif Variant == 3 then
            itemId = "dnk_ceph_cuirass"
        end
    elseif equippedRec.type == 2 then
        -- We don't care about anything but Armun-An, but if we find it, we should keep track of it
        if equippedRec.id ~= "bonemold_armun-an_pauldron_l" then
            return
        else
            foundArmun = true
        end
        -- Try to get the appropriate left pauldron
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_PauldronL_01"
        elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_PauldL_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_p_l"
        elseif Variant == 3 then
            itemId = "dnk_ceph_p_l"
        end
    elseif equippedRec.type == 3 then
        -- We don't care about anything but Armun-An, but if we find it, we should keep track of it
        if equippedRec.id ~= "bonemold_armun-an_pauldron_r" then
            return
        else
            foundArmun = true
        end
        -- Try to get the appropriate right pauldron
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_PauldronR_01"
        elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_PauldR_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_p_r"
        elseif Variant == 3 then
            itemId = "dnk_ceph_p_r"
        end
    elseif equippedRec.type == 4 then
        -- We don't care about anything but Bonemold
        if equippedRec.id ~= "bonemold_greaves" then
            return
        end
         -- Try to get the appropriate greaves
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_Greaves_01"
        elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_Greaves_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_greaves"
        elseif Variant == 3 then
            itemId = "dnk_ceph_greaves"
        end
    elseif equippedRec.type == 5 then
        -- We don't care about anything but Bonemold
        if equippedRec.id ~= "bonemold_boots" then
            return
        end
         -- Try to get the appropriate boots
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_Boots_01"
         elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_Boots_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_bts"
        elseif Variant == 3 then
            itemId = "dnk_ceph_bts"
        end
    elseif equippedRec.type == 9 then
        -- We don't care about anything but Bonemold
        if equippedRec.id ~= "bonemold_bracer_left" then
            return
        end
         -- Try to get the appropriate left gauntlet
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_BracerL_01"
        elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_GauntL_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_gaunt_l"
        elseif Variant == 3 then
            itemId = "dnk_ceph_gaunt_l"
        end
    elseif equippedRec.type == 10 then
        -- We don't care about anything but Bonemold
        if equippedRec.id ~= "bonemold_bracer_right" then
            return
        end
         -- Try to get the appropriate right gauntlet
        if (Variant == 1) and (foundMoleCrab == true) then
            itemId = "T_De_Molecrab_BracerR_01"
        elseif Variant == 1 then
            itemId = "T_De_TelvCephalopod_GauntR_01"
        elseif Variant == 2 then
            itemId = "_dnk_ceph_gaunt_r"
        elseif Variant == 3 then
            itemId = "dnk_ceph_gaunt_r"
        end
    else
        return
    end
    return itemId
end

local function checkHelmet(actor)
    -- Get the currently equipped helmet
    local equippedHelmet = equipped[actor.type.EQUIPMENT_SLOT.Helmet]

    -- Return if actor has no helmet
    if equippedHelmet == nil then
        return false
    end

    -- Get the currently equipped helmet record for data access
    local equippedHelmRec = equippedHelmet.type.record(equippedHelmet)

    -- Check if the actor has one of the Mole Crab helmets
    if moleCrabHelmets[equippedHelmRec.id] == nil then
        return false
    else
        return true
    end
end

-- try to replace the armor piece in chosen slot with a Cephalopod piece
local function replaceItem(actor, slot)
    -- Get the currently equipped item
    local equippedItem = equipped[slot]

    -- Don't put an item on an NPC that doesn't already have one of the same type
    if equippedItem == nil then
        return
    end

    -- Get the currently equipped item record for data access
    local equippedRec = equippedItem.type.record(equippedItem)

    -- Find me the item that I desire
    local itemId = findItem(equippedRec)

    -- We didn't find an item to swap
    if not itemId then
        return
    end

    -- Create the item...
    local newItem = world.createObject(itemId)

    -- Give this NPC the item!
    newItem:moveInto(actor.type.inventory(actor))
    actor:sendEvent("momw_telvanniArmor_equipItem", {itemObj = newItem, slot = equippedRec.type})

    -- Send the old item to Oblivion
    deleteObj(equippedItem)
end

local function onActorActive(actor)
    -- Already processed
    if processedNPCs[actor.id] then return end

    -- Don't dress an actor from a blacklisted mod
    if modBlacklist[actor.contentFile] then
        processedNPCs[actor.id] = true
        return
    end

    -- Don't dress a blacklisted actor
    if actorBlacklist[actor.recordId] then
        processedNPCs[actor.id] = true
        return
    end

    -- Don't dress an actor without an faction
    if actor.type.getFactions == nil then
        processedNPCs[actor.id] = true
        return
     end

    -- Don't dress an actor not in the Telvanni faction
    for _, faction in pairs(actor.type.getFactions(actor)) do
        if supportedFactions[faction] == nil then
            processedNPCs[actor.id] = true
            return
        end
    end

    -- We passed all the checks, so let's start with getting equipment of the actor
    equipped = actor.type.equipment(actor)

    -- Haven't found any Armun-An or Mole Crab pieces yet
    foundArmun = false
    foundMoleCrab = false

    -- if we have Tamriel Data, we can distribute Mole Crab armor as well
    if (Variant == 1) then
        foundMoleCrab = checkHelmet(actor)
    end

    -- Go through all of the Armun-An equipment slots except the helm
    actor:sendEvent("momw_telvanniArmor_startEquipProcess")
    replaceItem(actor, actor.type.EQUIPMENT_SLOT.Cuirass)
    replaceItem(actor, actor.type.EQUIPMENT_SLOT.LeftPauldron)
    replaceItem(actor, actor.type.EQUIPMENT_SLOT.RightPauldron)

    -- If we found an Armun-An equipment piece, let's replace the rest of the slots as well
    if foundArmun == true then
        replaceItem(actor, actor.type.EQUIPMENT_SLOT.Greaves)
        replaceItem(actor, actor.type.EQUIPMENT_SLOT.Boots)
        replaceItem(actor, actor.type.EQUIPMENT_SLOT.RightGauntlet)
        replaceItem(actor, actor.type.EQUIPMENT_SLOT.LeftGauntlet)
    end

    -- Pretty much done!
    actor:sendEvent("momw_telvanniArmor_finishEquipProcess")
    processedNPCs[actor.id] = true
end

local function onLoad(data)
    processedNPCs = data.processedNPCs
end

local function onSave()
    return {processedNPCs = processedNPCs}
end

local function addModToBlacklist(pluginNames)
    for _, name in pairs(pluginNames) do
        modBlacklist[string.lower(name)] = true
    end
end

local function addActorToBlacklist(actorIds)
    for _, id in pairs(actorIds) do
        actorBlacklist[string.lower(id)] = true
    end
end

-- Default blacklistings
addModToBlacklist({
        "tr_mainland.esm"
})
addActorToBlacklist({
        "telvanni guard_angler1",
        "telvanni guard_angler4"
})

return {
    engineHandlers = {
        onActorActive = onActorActive,
        onLoad = onLoad,
        onSave = onSave
    },
    interfaceName = "DynamicTelvanniArmor",
    interface = {
        AddActorToBlacklist = addActorToBlacklist,
        AddModToBlacklist = addModToBlacklist
    }
}
